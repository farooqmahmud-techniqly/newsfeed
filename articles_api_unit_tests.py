import unittest
import app
import json
from datetime import datetime


class ArticlesApiUnitTests(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def __get_url(self, url):
        return self.app.get(url)

    def test_ping(self):
        rv = self.__get_url("/api/articles/ping")
        data = json.loads(rv.data)
        self.assertEqual(data["title"], "Articles API")
        self.assertEqual(data["version"], "0.5.1")

        ts = data["ts"]
        now = datetime.utcnow().timestamp()
        self.assertGreaterEqual(now, ts)

    def test_articles_with_no_request_args_returns_default_feed(self):
        default_feed = b"www.cnn.com"
        rv = self.__get_url("/articles")
        assert default_feed in rv.data

if __name__ == "__main__":
    unittest.main()
