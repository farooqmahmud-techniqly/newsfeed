from flask import Flask, jsonify, render_template, request
import feedparser
from werkzeug import exceptions
from datetime import datetime
import json, urllib.parse, urllib.request

app = Flask(__name__)

__feeds = {"bbc": "http://feeds.bbci.co.uk/news/rss.xml",
           "cnn": "http://rss.cnn.com/rss/edition.rss",
           "fox": "http://feeds.foxnews.com/foxnews/latest"}


def __get_weather(query):
    if query is None:
        return None

    url = "http://api.openweathermap.org/data/2.5/weather?q={0}&units=imperial&appid=0bfd9c8ec382ac598e201a033426f6ef".format(
        urllib.parse.quote(query))

    data = urllib.request.urlopen(url).read()
    parsed = json.loads(data)
    weather = None

    if parsed.get("weather"):
        weather = {
            "description": parsed["weather"][0]["description"],
            "temperature": parsed["main"]["temp"],
            "city": parsed["name"]
        }

    return weather


@app.errorhandler(exceptions.NotFound)
def __not_found(feed):
    message = {"status": 404, "message": "Feed \"{0}\" not found.".format(feed)}
    response = jsonify(message)
    response.status_code = 404
    return response


def __get_articles(feed, as_json=True):
    requested_feed = __feeds.get(feed)

    if requested_feed is None:
        return __not_found(feed)

    feed = feedparser.parse(requested_feed)
    articles = []

    for entry in feed["entries"]:
        articles.append({"id": entry.get("id"), "title": entry.get("title"), "date": entry.get("published"),
                         "summary": entry.get("summary"), "link": entry.get("link")})

    if as_json:
        return jsonify(articles)

    return articles


@app.route("/api/articles/info")
def info():
    routes = []

    for r in app.url_map.iter_rules():
        routes.append({"methods": list(r.methods), "route": str(r)})

    return jsonify(routes)


@app.route("/api/articles/<feed>")
def get_articles(feed):
    return __get_articles(feed)


@app.route("/api/weather/<query>")
def get_weather(query):
    return jsonify(__get_weather(query))


@app.route("/articles/<feed>")
def view_articles_for_feed(feed, weather_query):
    articles = __get_articles(feed, False)
    weather = __get_weather(weather_query)
    return render_template("headlines.html", articles=articles, weather=weather)


@app.route("/articles")
def view_articles():
    query = request.args.get("feed")

    if not query or query.lower() not in __feeds:
        feed = "cnn"
    else:
        feed = query.lower()

    weather_query = request.args.get("weatherQuery")
    return view_articles_for_feed(feed, weather_query)


@app.route("/api/articles/ping")
def ping():
    return jsonify({"title": "Articles API", "version": "0.5.1", "ts": datetime.utcnow().timestamp()})


if __name__ == "__main__":
    app.run(port=7924, debug=True)
